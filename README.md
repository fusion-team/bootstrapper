# Bootstrapper

Set of CMake scripts based on [G2CK] that can check if you have all dependencies necessary to build the engine or download them if you don't.

It assumes you have a **build environment** available and **[CMake]** installed, so, before you start, go get them!

- If you're using Apple-made operating systems, you'll want Xcode with gcc at least version 4.8.
- If you're using Windows, We highly recommend [TDM-GCC] and the IDE of your choice.
- If you're using Linux, use your package-manager to set it up for you.
  - If you use a Linux distro with yum, check [here](#fedora-core)
  - If you use a Linux distro with pacman, check [here](#arch-linux)
  - In apt-based distros, run `apt-get install build-essential subversion automake autoconf cmake`
- Compiler and CMake must be in your `PATH`
- **[Java JDK]** and **[Apache Ant]** must be in your `PATH`
- For Android builds, you will need to download [Android SDK](https://developer.android.com/sdk/index.html) and [Android NDK](https://developer.android.com/ndk/downloads/index.html)

You will need a compiler that supports C++11

## Target layout

Bootstrapper will create an environment prepared for you to build the engine. The resulting layout when running Bootstrapper scripts is the following:

     - FUSION_ROOT/            CMake variable pointing to top-level directory do contain required libraries
       - android-project/:     Base for new android projects
       - lib/:                 Necessary libraries for the engine and also cmake modules
       - include/:             Header files for libraries and also for the engine
       - bin/:                 Tools needed for engine and game compilations
       - games/:               A convenient place to place all your games

`FUSION_ROOT` will also be used as install prefix when running make install on the engine

## Building (or downloading) your dependencies

Make sure you have CMake and your build environment ready, you are going to need them.

Create a `build` folder in the folder where this file is. Every interaction with Bootstrapper will happen inside this folder. Open a command prompt and change directory to the `build` folder that you just created. 

After you're inside `build`, execute these commands:

### Linux and Mac:

For Desktop Games:

    cmake .. -DFUSION_ROOT=/path/to/engine/root
    make new-game GAME_NAME="MY_GAME_NAME" 
    cd /path/to/engine/root/games/MY_GAME_NAME
    mkdir build
    cd build
    cmake .. 
    make 
    
For Games with Android Support:

    cmake .. -DFUSION_ROOT=/path/to/engine/root
    make new-game GAME_NAME="MY_GAME_NAME" ANDROID=YES
    cd /path/to/engine/root/games/MY_GAME_NAME
    mkdir build
    cd build
    cmake .. -DANDROID_NDK=/path/for/android/ndk -DANDROID_SDK=/path/for/android/sdk -ANDROID_API_VERSION=NN -DAPK_NAME=my_apk_name -DGAME_PACKAGE=my.pkg.name
    make android-game
    
### Windows:

*MinGW:*

For Desktop Games:

    cmake -G "MinGW Makefiles" .. -DFUSION_ROOT=/path/to/engine/root
    make new-game GAME_NAME="MY_GAME_NAME" 
    cd /path/to/engine/root/games/MY_GAME_NAME
    mkdir build
    cd build
    cmake -G "MinGW Makefiles" .. 
    mingw32-make 
    
For Games with Android Support:

    cmake -G "MinGW Makefiles" .. -DFUSION_ROOT=/path/to/engine/root
    mingw32-make new-game GAME_NAME="MY_GAME_NAME" ANDROID=YES
    cd /path/to/engine/root/games/MY_GAME_NAME
    mkdir build
    cd build
    cmake -G "MinGW Makefiles" .. -DANDROID_NDK=/path/for/android/ndk -DANDROID_SDK=/path/for/android/sdk -ANDROID_API_VERSION=NN -DAPK_NAME=my_apk_name -DGAME_PACKAGE=my.pkg.name
    mingw32-make android-game

*MSYS (e.g, inside MSYS Bash)*

For Desktop Games:

    cmake -G "MSYS Makefiles" .. -DFUSION_ROOT=/path/to/engine/root
    make new-game GAME_NAME="MY_GAME_NAME" 
    cd /path/to/engine/root/games/MY_GAME_NAME
    mkdir build
    cd build
    cmake -G "MSYS Makefiles" .. 
    make 
    
For Games with Android Support:

    cmake -G "MSYS Makefiles" .. -DFUSION_ROOT=/path/to/engine/root
    make new-game GAME_NAME="MY_GAME_NAME" ANDROID=YES
    cd /path/to/engine/root/games/MY_GAME_NAME
    mkdir build
    cd build
    cmake -G "MSYS Makefiles" .. -DANDROID_NDK=/path/for/android/ndk -DANDROID_SDK=/path/for/android/sdk -ANDROID_API_VERSION=NN -DAPK_NAME=my_apk_name -DGAME_PACKAGE=my.pkg.name
    make android-game
    
    
# Appendix

## Dependencies

Bootstrapper will build all the needed libraries, but it depends on your platform being ready to do so. This section provides a little help with some specific Linux distributions.

### Fedora Core

You will need to install a list of packages to be able to properly compile engine and its dependencies. These are the commands you will need to run (**as root**)

    yum groupinstall 'Development Tools' 'C Development Tools and Libraries'
    yum install gcc-g++ patch subversion automake autoconf 
    
### Arch Linux

You will need to install a list of packages to be able to properly compile engine and its dependencies. These are the commands you will need to run (**as root**)


*If you're in a multlib system do:*

    pacman -S gcc-multilib autoconf automake binutils bison fakeroot file findutils flex gawk gettext grep groff 
              gzip libtool m4 make patch pkg-config sed sudo texinfo util-linux which
    
*If not, do:*

    pacman -S binutils base-devel
    

[SVN]:https://subversion.apache.org/packages.html
[CMake]: http://cmake.org/cmake/resources/software.html
[TDM-GCC]: http://tdm-gcc.tdragon.net/
[G2CK]: http://github.com/Gear2D/g2ck
[Apache Ant]: http://ant.apache.org/
[Java JDK]: http://www.oracle.com/technetwork/java/javase/downloads/index.html