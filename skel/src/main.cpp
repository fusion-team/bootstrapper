#include "Fusion.h"

#ifdef __ANDROID__
#define main SDL_main
#endif

extern "C" int main(int argc, char* argv[])
{
	/* initializes logtrace logging facilities to log and trace everything */
	logtrace::globalverb(logtrace::maximum);

	/* creates a tracing object for this scope */
	modinfo("main");

	/* gets the engine instance */
	Fusion::Engine & engine = Fusion::Engine::instance();

	/* obtain or instantiate a scene called "main" */
	engine.scene("main");

	/* runs the engine, returns when finished */
	engine.run();

	return 0;
}
