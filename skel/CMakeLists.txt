cmake_minimum_required(VERSION 3.1.3)
set(CMAKE_CXX_STANDARD 11)

# FUSION_ROOT is a variable pointing to where bootstrapper
# installed all dependencies. You can override it by
# passing -DFUSION_ROOT=/other/path in cmake command line
set(FUSION_ROOT "${CMAKE_SOURCE_DIR}/../.." CACHE PATH "Path where bootstrapper installed your dependencies")

# ANDROID_PROJECT_ROOT points to a folder called android-project
# generated when you ran bootstrapper with make android-deps
set(ANDROID_PROJECT_ROOT "${FUSION_ROOT}/android-project" CACHE PATH "Path where bootstrapper generated the base android-project")

# Asks CMake to try to find libraries in ${FUSION_ROOT} too
list(APPEND CMAKE_PREFIX_PATH "${FUSION_ROOT}")

# @GAME_TARGET@ is the executable name for "@GAME_NAME@" 
project(@GAME_TARGET@)

# Fusion requires C++11 at all times
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# FindPackage utilities. This is the place where you might put custom
# Find<XXX>.cmake files (where XXX is the name of a package) to find
# packages. Consult CMake documentation on how to write those.
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake-modules")

# Locally include the engine. This allows you to make changes to the engine
# while developing your code. It also makes it easy to debug stuff.
add_subdirectory(fusion-engine)

# Add fusion-engine/src as an include subdirectory so that
# your game can find engine headers
include_directories(${FUSION_INCLUDE_DIRS})

# Where the code for your game is going to be placed.
# Consult CMakeLists.txt inside of src/ to learn how to add new
# files to your project.
add_subdirectory(src)
if (DEFINED ANDROID_NDK AND DEFINED ANDROID_SDK AND DEFINED ANDROID_API_VERSION)
	if(WIN32)
		set(ANT_NAMES ant.bat)
		set(ANDROID_BIN_NAMES android.bat)
		set(NDK_BIN_NAMES ndk-build.cmd)
	else()
		set(ANT_NAMES ant)
		set(ANDROID_BIN_NAMES android)
		set(NDK_BIN_NAMES ndk-build)
	endif()

	#JAVA JDK
	find_package(Java COMPONENTS Development)
	if(NOT Java_Development_FOUND)
		message(FATAL_ERROR "Could not find Java JDK. Please install it and add it to PATH.")
	else()
        if(WIN32)
    		get_filename_component(JDK_PATH_CMAKE ${Java_JAVAC_EXECUTABLE} DIRECTORY)
		    get_filename_component(JDK_PATH_CMAKE ${JDK_PATH_CMAKE}/.. ABSOLUTE)
        else()
            get_filename_component(JDK_PATH_CMAKE ${Java_JAVAC_EXECUTABLE} REALPATH)
		    get_filename_component(JDK_PATH_CMAKE ${JDK_PATH_CMAKE}/../.. ABSOLUTE)
        endif()
		file(TO_NATIVE_PATH ${JDK_PATH_CMAKE} JDK_PATH)
		message(STATUS "Java JDK found in : ${JDK_PATH}")
	endif()

	if(NOT ANT_PATH)
		set(ANT_PATHS "ENV PATH")
	else()
		set(ANT_PATHS "${ANT_PATH}")
	endif()

	# Try to find ANT
	message(STATUS "Looking for ANT binaries")
	find_program(ANT 
		NAMES ${ANT_NAMES} 
		DOC "Software tool for automating software build processes"
		PATHS ${ANT_PATHS}
		CMAKE_FIND_ROOT_PATH_BOTH
	)

	if(NOT ANT)
		if(NOT ANT_PATH)
			message(STATUS "Could not find the ANT executable in system path.")
		else()
			message(STATUS "Could not find the ANT executable in ${ANT_PATH} nor in system path.")
		endif()
		message(FATAL_ERROR "Could not find Apache's ant executable. Please install it and add it to PATH.")
	endif()
	message(STATUS "Ant found: ${ANT}")
	
	#CHECKING VALID ANDROID SDK PATH
	message(STATUS "Looking for Android SDK binaries")
	find_program(SDK_ANDROID_BIN
		NAMES ${ANDROID_BIN_NAMES}
		PATHS ${ANDROID_SDK} ${ANDROID_SDK}/tools
		DOC "Android Software Development Kit"
		NO_DEFAULT_PATH
	)

	if(NOT SDK_ANDROID_BIN)
		message(STATUS "Could not find the android sdk-manager executable insde ${ANDROID_SDK}")
		message(FATAL_ERROR "Please correct ANDROID_SDK to a valid path.")
	endif()
	message(STATUS "Valid Android SDK Path")
	
	#CHECKING VALID ANDROID NDK PATH
	message(STATUS "Looking for Android NDK binaries")
	find_program(NDK_BUILD_BIN
		NAMES ${NDK_BIN_NAMES}
		PATHS ${ANDROID_NDK}
		DOC "Program to build native code for Android"
		NO_DEFAULT_PATH
	)

	if(NOT NDK_BUILD_BIN)
		message(STATUS "Could not find the ndk-build executable inside ${ANDROID_NDK}.")
		message(FATAL_ERROR "Please correct ANDROID_NDK to a valid path.")
	endif()
	message(STATUS "Found ndk-build at ${ANDROID_NDK}")

	if(NOT GAME_PACKAGE)
	    message(STATUS "Please provide a valid GAME_PACKAGE for android game. (ex: my.pkg.game).")
		message(FATAL_ERROR "No package name provided")
	endif()	
	message(STATUS "Package Name: ${GAME_PACKAGE}")

	if(NOT APK_NAME)
	    message(STATUS "Please provide a valid APK_NAME for android game. (ex: apk_name).")
		message(FATAL_ERROR "No APK name provided")
	endif()
	message(STATUS "APK Name: ${APK_NAME}")	


    message(STATUS "Will build for android")
    add_custom_target(android-game
        COMMAND
        ${CMAKE_COMMAND}
        -DGAME_NAME="@GAME_TARGET@"
        -DGAME_ROOT=${CMAKE_SOURCE_DIR}
        -DFUSION_ROOT=${FUSION_ROOT}
        -DNDK_BUILD=${NDK_BUILD_BIN}
        -DANDROID_SDK=${ANDROID_SDK}			
        -DANDROID_API_VERSION=${ANDROID_API_VERSION}
		-DANDROID_GAME_PACKAGE=${GAME_PACKAGE}
        -DGAME_ACTIVITY="@GAME_TARGET@"
		-DAPK_NAME=${APK_NAME}
        -DAPK_MODE="\${APK_MODE}"
		-DJDK_HOME=${JDK_PATH}
        -DANT=${ANT}
        -DVERBOSE="\${VERBOSE}"
        -P ${CMAKE_SOURCE_DIR}/cmake-scripts/android-build.cmake

    )

    add_custom_target(clean-android-game COMMAND  ${ANDROID_NDK}/ndk-build clean -C ${CMAKE_SOURCE_DIR}/android-project/jni )
	
endif()
