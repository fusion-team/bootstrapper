cmake_minimum_required(VERSION 3.0)

# Sets used paths
set(GAME_ANDROID_ROOT ${GAME_ROOT}/android-project)     # android-project for this game
set(FUSION_ANDROID_ROOT ${FUSION_ROOT}/android-project) # base android-project for all games

set(GAME_ANDROID_SRC ${GAME_ROOT}/android-src)     # android-src for this game
set(GAME_ANDROID_JAVA_SRC ${GAME_ANDROID_SRC}/java-src)     # android-src/java-src for this game

function(MKLINK TARGET_PATH LINK_PATH)
	#Normalize the paths

	file(TO_NATIVE_PATH ${LINK_PATH} NATIVE_LINK_PATH)
	file(TO_NATIVE_PATH ${TARGET_PATH} NATIVE_TARGET_PATH)
    if( NOT EXISTS ${NATIVE_TARGET_PATH})
        message(FATAL_ERROR "Target Path do not exists: ${NATIVE_TARGET_PATH}")
    endif()

	message(STATUS "${NATIVE_TARGET_PATH} -----> ${NATIVE_LINK_PATH}")
	#Identify OS link command
	if (WIN32)
		# Need to check if it will work or need to use /J /D
		# ref: http://www.howtogeek.com/howto/16226/complete-guide-to-symbolic-links-symlinks-on-windows-or-linux/
		execute_process(COMMAND cmd /c mklink /J ${NATIVE_LINK_PATH} ${NATIVE_TARGET_PATH})
	else()
		execute_process(COMMAND ln -sf ${NATIVE_TARGET_PATH} ${NATIVE_LINK_PATH})
	endif()
endfunction()

function(CONFIGURE_FILES)
    configure_file(${GAME_ANDROID_SRC}/build.xml.in ${GAME_ANDROID_ROOT}/build.xml @ONLY)
    configure_file(${GAME_ANDROID_SRC}/project.properties.in ${GAME_ANDROID_ROOT}/project.properties @ONLY)
    configure_file(${GAME_ANDROID_SRC}/AndroidManifest.xml.in ${GAME_ANDROID_ROOT}/AndroidManifest.xml)
    configure_file(${GAME_ANDROID_SRC}/jni/Application.mk.in ${GAME_ANDROID_ROOT}/jni/Application.mk @ONLY)
    configure_file(${GAME_ANDROID_SRC}/Android.mk.in ${GAME_ROOT}/src/Android.mk @ONLY)
    configure_file(${GAME_ANDROID_SRC}/res/values/strings.xml.in ${GAME_ANDROID_ROOT}/res/values/strings.xml @ONLY)


    set(SDL_JAVA_PKG org/libsdl/app)
    set(SDL_ACTIVITY_FOLDER ${GAME_ANDROID_JAVA_SRC}/${SDL_JAVA_PKG})
    file(MAKE_DIRECTORY ${GAME_ROOT}/java-src/${SDL_JAVA_PKG})

    configure_file(${SDL_ACTIVITY_FOLDER}/SDLActivity.java.in ${GAME_ROOT}/java-src/${SDL_JAVA_PKG}/SDLActivity.java @ONLY)

    string(REPLACE "." "/" ANDROID_GAME_PACKAGE_DIR ${ANDROID_GAME_PACKAGE})
    message(STATUS "Creating Android package directories: ${GAME_ROOT}/java-src/${ANDROID_GAME_PACKAGE_DIR}")
    file(MAKE_DIRECTORY ${GAME_ROOT}/java-src/${ANDROID_GAME_PACKAGE_DIR})
    message(STATUS "Creating game java file: ${GAME_ROOT}/java-src/${ANDROID_GAME_PACKAGE_DIR}/${GAME_NAME}.java")
    configure_file( ${GAME_ANDROID_JAVA_SRC}/GameActivity.java.in ${GAME_ROOT}/java-src/${ANDROID_GAME_PACKAGE_DIR}/${APK_NAME}.java)

endfunction()	

function(MK_ANDROID_LINKS)
		if(NOT EXISTS ${GAME_ANDROID_ROOT}/assets)
    		if(NOT EXISTS ${GAME_ROOT}/assets)
                message(STATUS "Creating Assets folder: ${GAME_ROOT}/assets")
                file(MAKE_DIRECTORY ${GAME_ROOT}/assets)
            endif()
			message(STATUS "Creating link for the Assets folder")
			MKLINK( ${GAME_ROOT}/assets ${GAME_ANDROID_ROOT}/assets )
		endif()

		if(NOT EXISTS ${GAME_ANDROID_ROOT}/jni/fusion-engine)
			message(STATUS "Creating link for the Fusion Engine")
			MKLINK(${GAME_ROOT}/fusion-engine ${GAME_ANDROID_ROOT}/jni/fusion-engine)
		endif()

		if(NOT EXISTS ${GAME_ANDROID_ROOT}/src)
			message(STATUS "Creating link for the Java Sources")

            if(NOT EXISTS ${GAME_ROOT}/java-src)
                file(MAKE_DIRECTORY ${GAME_ROOT}/java-src)
            endif()
			MKLINK(${GAME_ROOT}/java-src ${GAME_ANDROID_ROOT}/src)
		endif()

		if(NOT EXISTS ${GAME_ANDROID_ROOT}/jni/src)
			message(STATUS "Creating link for the Game Sources")
			MKLINK(${GAME_ROOT}/src ${GAME_ANDROID_ROOT}/jni/src)
		endif()
endfunction()

if(NOT EXISTS ${GAME_ANDROID_ROOT})
	if(EXISTS ${FUSION_ANDROID_ROOT})
        # copy base android project to our folder
        message(STATUS "Copying base android-project to local game folder")
        message(STATUS "${FUSION_ANDROID_ROOT}/ -> ${GAME_ANDROID_ROOT}/")
        execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${FUSION_ANDROID_ROOT}/ ${GAME_ANDROID_ROOT}/)
        message(STATUS "Configuring Android project files")
		#make links
		MK_ANDROID_LINKS()
	else()
        message(STATUS 
"Fusion android-project base folder was not found.
We were expecting the folder ${FUSION_ANDROID_ROOT} to exist.
Are you sure you passed variables ANDROID_NDK and ANDROID_SDK properly to
bootstrapper's 'make new-game' command?"
        )
        message(FATAL_ERROR "Fusion's android-project root was not found")
	endif()
else()
	#make links
	MK_ANDROID_LINKS()
endif()

if(NOT ANDROID_API_VERSION)
	message(FATAL_ERROR "Please set ANDROID_API_VERSION to a valid one.")
endif()

CONFIGURE_FILES()

message(STATUS "Building android jni libraries")

if(VERBOSE)
	set(NDK_VERBOSE_MODE "V=1")
	set(ANT_VERBOSE_MODE "-v")
    message("${NDK_BUILD} -C ${GAME_ANDROID_ROOT} ${NDK_VERBOSE_MODE}")
endif()

execute_process(COMMAND ${NDK_BUILD} -C ${GAME_ANDROID_ROOT} ${NDK_VERBOSE_MODE} RESULT_VARIABLE res_var)

if(NOT "${res_var}" STREQUAL "0")
  # do something here about the failed "process" call...
  message(FATAL_ERROR "NDK Build process failed...")
endif()

#TODO create APK (ant or gradle ?)
if(NOT APK_MODE)
	message(STATUS "APK_MODE not defined. APK_MODE set to debug.")
	set(ANT_APK_MODE debug)
else()
	string(TOLOWER ${APK_MODE} APK_MODE_LOWERCASE)
	if(NOT "${APK_MODE_LOWERCASE}" STREQUAL "debug")
		if(NOT "${APK_MODE_LOWERCASE}" STREQUAL "release")
			message(STATUS "Invalid APK_MODE. APK_MODE set to debug.")
			set(ANT_APK_MODE debug)
		else()
			message(STATUS "APK_MODE set to release.")
			set(ANT_APK_MODE release)
		endif()
	else()
			message(STATUS "APK_MODE set to debug.")
			set(ANT_APK_MODE debug)
	endif()	
endif()

if(WIN32)
	string(REPLACE "\\" "" JDK_HOME_WINDOWS ${JDK_HOME})
	set(ENV{JAVA_HOME} ${JDK_HOME_WINDOWS})
endif()	

set(ENV{ANDROID_HOME} ${ANDROID_SDK})
execute_process(COMMAND ${ANT} ${ANT_VERBOSE_MODE} ${ANT_APK_MODE} -f ${GAME_ANDROID_ROOT})
