LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := ogg

LOCAL_CFLAGS    := \
    -DFIXED_POINT -DUSE_KISS_FFT -DEXPORT="" -UHAVE_CONFIG_H \
    -D_ANDROID -O3 
	
LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include 
	
LOCAL_SRC_FILES := \
	 src/bitwise.c \
	 src/framing.c
 
LOCAL_EXPORT_LDLIBS :=     
	
include $(BUILD_STATIC_LIBRARY)
