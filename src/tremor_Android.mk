LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES =      \
		block.c        \
		codebook.c     \
        floor0.c       \
        floor1.c       \
		info.c         \
        mapping0.c     \
        mdct.c         \
		registry.c     \
		res012.c       \
		sharedbook.c   \
        synthesis.c    \
		vorbisfile.c   \
		

LOCAL_CFLAGS+= -O3 -I$(LOCAL_PATH) -DHAVE_ALLOCA_H

ifeq ($(TARGET_ARCH),arm)
	LOCAL_CFLAGS+= -D_ARM_ASSEM_
endif
        
LOCAL_C_INCLUDES:= \
		$(LOCAL_PATH)/../libogg/include

LOCAL_ARM_MODE := arm

LOCAL_MODULE := libvorbisidec

LOCAL_SHARED_LIBRARIES := ogg
LOCAL_STATIC_LIBRARIES := ogg

LOCAL_EXPORT_LDLIBS :=

include $(BUILD_STATIC_LIBRARY)
