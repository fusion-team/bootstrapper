include(ExternalProject)

ExternalProject_Add(
    glm
    PREFIX ${CMAKE_BINARY_DIR}
    INSTALL_DIR ${FUSION_ROOT}
    URL https://github.com/g-truc/glm/archive/master.zip
    DOWNLOAD_NAME glm-master.zip
    DOWNLOAD_DIR ${DOWNLOAD_DIR}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${FUSION_ROOT} -DCMAKE_INSTALL_LIBDIR=lib
)
