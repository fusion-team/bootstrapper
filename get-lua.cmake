include(ExternalProject)

ExternalProject_Add(
	lua
	PREFIX ${CMAKE_BINARY_DIR}
	INSTALL_DIR ${FUSION_ROOT}
	URL https://github.com/LuaDist/lua/archive/master.zip
	DOWNLOAD_NAME lua-master.zip
	DOWNLOAD_DIR ${DOWNLOAD_DIR}
	CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${FUSION_ROOT} -DBUILD_SHARED_LIBS=FALSE # Lua as a static lib is more... relevant.
)
