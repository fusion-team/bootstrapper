cmake_minimum_required(VERSION 3.0)

message(STATUS "Acquiring project dependencies for android")

## Download remaing deps
if(WIN32)
	message(STATUS "Downloading SDL2 sources for android")
	set(SDL2_VERSION 2.0.3)
	set(SDL2_IMAGE_VERSION 2.0.0)
	set(SDL2_TTF_VERSION 2.0.12)
	set(SDL2_SRC SDL2-${SDL2_VERSION})
	set(SDL2_IMAGE_SRC SDL2_image-${SDL2_IMAGE_VERSION})
	set(SDL2_TTF_SRC SDL2_ttf-${SDL2_TTF_VERSION})
	include(${BOOTSTRAP_DIR}/cmake-scripts/get-windows-android-deps.cmake)
else()
	set(SDL2_SRC sdl2)
	set(SDL2_IMAGE_SRC sdl2-image)
	set(SDL2_TTF_SRC sdl2-ttf)
endif()

set(OPENAL_DIR ${CMAKE_BINARY_DIR}/src/openal-soft-android/)
set(OPENAL_SRC_TGZ ${CMAKE_BINARY_DIR}/src/openal-soft-android.tar.gz)
set(GLM_DIR ${CMAKE_BINARY_DIR}/src/glm)

if(NOT EXISTS "${OPENAL_SRC_TGZ}")
	message(STATUS "Downloading OpenAL-Soft for android")
    file(DOWNLOAD http://repo.or.cz/w/openal-soft/android.git/snapshot/master.tar.gz ${OPENAL_SRC_TGZ} SHOW_PROGRESS STATUS status)
    if(status)
        message(STATUS "Extracting OpenAL-Soft for android sources")
        file(MAKE_DIRECTORY ${OPENAL_DIR})
        execute_process(COMMAND ${CMAKE_COMMAND} -E tar xzf ${OPENAL_SRC_TGZ} WORKING_DIRECTORY ${OPENAL_DIR})
		file(GLOB inFolder RELATIVE ${OPENAL_DIR} "${OPENAL_DIR}/*")
		file(RENAME ${OPENAL_DIR}/${inFolder} ${OPENAL_DIR}/master )
    else()
        message(FATAL_ERROR "Failed to download OpenAL-Soft sources")
    endif()
else()
    if (NOT EXISTS "${OPENAL_DIR}")
        message(STATUS "Extracting OpenAL-Soft for android sources")
        file(MAKE_DIRECTORY ${OPENAL_DIR})
        execute_process(COMMAND ${CMAKE_COMMAND} -E tar xzf ${OPENAL_SRC_TGZ} WORKING_DIRECTORY ${OPENAL_DIR})
		file(GLOB inFolder RELATIVE ${OPENAL_DIR} "${OPENAL_DIR}/*")
		file(RENAME ${OPENAL_DIR}/${inFolder} ${OPENAL_DIR}/master )
	else()
		set(inFolder "")
		file(GLOB inFolder RELATIVE ${OPENAL_DIR} "${OPENAL_DIR}/*")
		message(STATUS "inFolder => ${inFolder}")
		if("${inFolder}" STREQUAL "")
			execute_process(COMMAND ${CMAKE_COMMAND} -E tar xzf ${OPENAL_SRC_TGZ} WORKING_DIRECTORY ${OPENAL_DIR})
			file(GLOB inFolder RELATIVE ${OPENAL_DIR} "${OPENAL_DIR}/*")		
		endif()
		file(RENAME ${OPENAL_DIR}/${inFolder} ${OPENAL_DIR}/master )
    endif()
endif()

### CREATE ANDROID LIBS
set(ANDROID_PROJECT_DIR ${CMAKE_BINARY_DIR}/src/android-project)

# Copying android project from SDL2 source
message(STATUS "Creating base android-project folder")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/src/${SDL2_SRC}/android-project/  ${ANDROID_PROJECT_DIR}/ )

#Copying deps to android project
message(STATUS "Copying lua sources")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/src/lua/  ${ANDROID_PROJECT_DIR}/jni/lua OUTPUT_QUIET)

message(STATUS "Copying SDL sources")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/src/${SDL2_SRC}/  ${ANDROID_PROJECT_DIR}/jni/SDL OUTPUT_QUIET)

message(STATUS "Copying SDL2_ttf sources")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/src/${SDL2_TTF_SRC}/  ${ANDROID_PROJECT_DIR}/jni/SDL2_ttf OUTPUT_QUIET)

message(STATUS "Copying SDL2_image sources")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/src/${SDL2_IMAGE_SRC}/  ${ANDROID_PROJECT_DIR}/jni/SDL2_image OUTPUT_QUIET)

message(STATUS "Copying GLM sources")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${GLM_DIR}/  ${ANDROID_PROJECT_DIR}/jni/glm OUTPUT_QUIET)

message(STATUS "Copying OpenAL soft sources")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${OPENAL_DIR}/master/  ${ANDROID_PROJECT_DIR}/jni/openal)

#Copying Android.mk to specific deps
message(STATUS "Copying custom Android.mk to specific deps")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/lua_Android.mk  ${ANDROID_PROJECT_DIR}/jni/lua/Android.mk OUTPUT_QUIET)
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/openal_soft_Android.mk  ${ANDROID_PROJECT_DIR}/jni/openal/Android.mk OUTPUT_QUIET)

# APPLY PATCHES TO BUILD STATICALY
message(STATUS "Applying patches to build libs statically")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/SDL_Android.mk ${ANDROID_PROJECT_DIR}/jni/SDL/Android.mk)
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/SDL2_image_Android.mk ${ANDROID_PROJECT_DIR}/jni/SDL2_image/Android.mk)
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/SDL2_ttf_Android.mk ${ANDROID_PROJECT_DIR}/jni/SDL2_ttf/Android.mk)
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/luaconf.h ${ANDROID_PROJECT_DIR}/jni/lua/src/luaconf.h)
execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${BOOTSTRAP_DIR}/src/openal_android_jni_Android.mk ${ANDROID_PROJECT_DIR}/jni/openal/android/jni/Android.mk )

message(STATUS "Removing unecessary files/folders")
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory  ${ANDROID_PROJECT_DIR}/jni/src)
execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory  ${ANDROID_PROJECT_DIR}/src)
#TODO
#Make a better way to build and provide static libs
#### ANDROID STANDALONE TOOLCHAIN
# Android defines
#set(ANDROID_ROOT ${FUSION_ROOT}/android-root)
set(ANDROID_PROJECT ${FUSION_ROOT}/android-project)

#set(CREATE_ANDROID_STANDALONE_TOLLCHAIN ${ANDROID_NDK}/build/tools/make-standalone-toolchain.sh)
#set(NDK_BUILD ${ANDROID_NDK}/ndk-build)
#set(ANDROID_PLATFORM android-19)
#set(ANDROID_ARCH arm) # options: arm, x86, mips, arm64, x86_64 or mips64
#set(NDK_INCLUDE ${ANDROID_NDK}/platforms/${ANDROID_PLATFORM}/arch-arm/usr/include )

#message(STATUS "Creating Stanadlone Toolchain")
#execute_process(COMMAND ${CREATE_ANDROID_STANDALONE_TOLLCHAIN} --install-dir=${ANDROID_ROOT} --platform=${ANDROID_PLATFORM} --arch=${ANDROID_ARCH})

# BUILD
#message(STATUS "Building libs")
#message(STATUS "${NDK_BUILD} -C  ${ANDROID_PROJECT_DIR}/jni/")
#execute_process(COMMAND ${NDK_BUILD} -C  ${ANDROID_PROJECT_DIR}/jni/)

#COPY LIBS TO FUSION_ROOT
#message(STATUS "Copying libs to Standalone Toolchain folder")
#execute_process(COMMAND ${CMAKE_COMMAND} -E copy  ${ANDROID_PROJECT_DIR}/obj/local/armeabi-v7a/liblua.a ${ANDROID_ROOT}/arm-linux-androideabi/lib/liblua.a)
#execute_process(COMMAND ${CMAKE_COMMAND} -E copy  ${ANDROID_PROJECT_DIR}/obj/local/armeabi-v7a/libogg.a ${ANDROID_ROOT}/arm-linux-androideabi/lib/libogg.a)
#execute_process(COMMAND ${CMAKE_COMMAND} -E copy  ${ANDROID_PROJECT_DIR}/obj/local/armeabi-v7a/libSDL2.a ${ANDROID_ROOT}/arm-linux-androideabi/lib/libSDL2.a)
#execute_process(COMMAND ${CMAKE_COMMAND} -E copy  ${ANDROID_PROJECT_DIR}/obj/local/armeabi-v7a/libSDL2_image.a ${ANDROID_ROOT}/arm-linux-androideabi/lib/libSDL2_image.a) 
#execute_process(COMMAND ${CMAKE_COMMAND} -E copy  ${ANDROID_PROJECT_DIR}/obj/local/armeabi-v7a/libSDL2_ttf.a ${ANDROID_ROOT}/arm-linux-androideabi/lib/libSDL2_ttf.a)
#execute_process(COMMAND ${CMAKE_COMMAND} -E copy  ${ANDROID_PROJECT_DIR}/obj/local/armeabi-v7a/libvorbisidec.a ${ANDROID_ROOT}/arm-linux-androideabi/lib/libvorbisidec.a)

message(STATUS "Copying generated android-project to ${FUSION_ROOT} (this might take a while)")
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory  ${ANDROID_PROJECT_DIR} ${ANDROID_PROJECT})
