cmake_minimum_required(VERSION 3.0)

# Do not allow project named test
string(TOLOWER ${GAME_NAME} GAME_NAME_LOWERCASE)
if (${GAME_NAME_LOWERCASE} STREQUAL "test")
    message(STATUS "'test' is a reserved CMake word and cannot be used as your game/project name.")
    message(STATUS "Please chose another name for your game other than ${GAME_NAME}.")
    message(FATAL_ERROR "Unacceptable project name")
endif()

# Produces name that can be a folder
string(MAKE_C_IDENTIFIER ${GAME_NAME} GAME_TARGET)
set(GAME_ROOT ${FUSION_ROOT}/game/${GAME_TARGET})

set(SUPPORT_ANDROID "no")
if(ENABLE_ANDROID AND NOT DISABLE_ANDROID)
    set(SUPPORT_ANDROID "yes")
endif()

# Copy skeleton to target
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${SKEL_DIR} ${GAME_ROOT}/)

# Replaces @VAR@ with our ${VAR} in these files
configure_file(${GAME_ROOT}/CMakeLists.txt ${GAME_ROOT}/CMakeLists.txt @ONLY)
configure_file(${GAME_ROOT}/src/CMakeLists.txt ${GAME_ROOT}/src/CMakeLists.txt @ONLY)

# Copy fusion engine locally
execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${FUSION_ROOT}/fusion-engine ${GAME_ROOT}/fusion-engine)

if ("${SUPPORT_ANDROID}" STREQUAL "yes")
	execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${BOOTSTRAP_DIR}/src/android-src ${GAME_ROOT}/android-src)
	set(ANDROID_DEFINES "-DANDROID_SDK=<path/to/sdk> -DANDROID_NDK=<path/to/ndk> -DANDROID_API_VERSION=<api_version_number> -DAPK_NAME=<my_apk_name> -DGAME_PACKAGE=<my.pkg.name>" )
	set(ANDROID_GAME_OPT android-game )
endif()

message("")
message(STATUS "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --")
message("")
message("Summary:")
message(STATUS "Game name:                ${GAME_NAME}")
message(STATUS "Game target:              ${GAME_TARGET}")
message(STATUS "Game folder:              ${GAME_ROOT}")
message(STATUS "Support for android:      ${SUPPORT_ANDROID}")
message("")
message("Useful information:")
message("  To build your game for android, you still need to point to")
message("  Android SDK and Android NDK when using 'cmake' to configure it:")
message("")
message("  cd ${GAME_ROOT}")
message("  mkdir build && cd build")
if(WIN32)
    message("  If you are on MinGW environment")
    message("  cmake -G \"MinGW Makefiles\" .. ${ANDROID_DEFINES}")
    message("  mingw32-make ${ANDROID_GAME_OPT}")
    message("")    
    message("  If you are on MSYS environment (e.g, inside MSYS Bash)")
    message("  cmake -G \"MSYS Makefiles\" .. ${ANDROID_DEFINES}")
    message("  make ${ANDROID_GAME_OPT}")
else()
    message("  cmake .. ${ANDROID_DEFINES}")
    message("  make ${ANDROID_GAME_OPT}")
endif()
message("")
message(STATUS "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --")

