find_package(Git QUIET)
include(ExternalProject)
if (NOT GIT_FOUND)
	message(WARNING "If you intend to develop fusion-engine, you should get git installed")
	message(WARNING "You won't be able to commit or push your changes.")
	ExternalProject_Add(
		fusion-engine
		PREFIX ${CMAKE_BINARY_DIR}
		INSTALL_DIR ${FUSION_ROOT}
		SOURCE_DIR ${FUSION_ROOT}/fusion-engine
		URL https://gitlab.com/fusion-team/fusion-engine/repository/archive.zip
		DOWNLOAD_NAME fusion-engine-master.zip
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${FUSION_ROOT} -DBUILD_SHARED_LIBS=FALSE -DCMAKE_BUILD_TYPE=Release
	)
else()
	ExternalProject_Add(
		fusion-engine
		PREFIX ${CMAKE_BINARY_DIR}
		INSTALL_DIR ${FUSION_ROOT}
		SOURCE_DIR ${FUSION_ROOT}/fusion-engine
		GIT_REPOSITORY https://gitlab.com/fusion-team/fusion-engine.git
		DOWNLOAD_DIR ${DOWNLOAD_DIR}
		CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${FUSION_ROOT} -DBUILD_SHARED_LIBS=FALSE -DCMAKE_BUILD_TYPE=Release
	)
endif()

add_dependencies(fusion-engine glm lua sdl2 sdl2-image openal sdl2-ttf)




