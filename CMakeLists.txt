project(bootstrap)
cmake_minimum_required(VERSION 3.0)
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake-modules")

get_filename_component(POSSIBLE_ROOT ${CMAKE_SOURCE_DIR}/../ ABSOLUTE)

# Get the external root path
set(FUSION_ROOT ${POSSIBLE_ROOT} CACHE PATH "Place where environment will be created")
set(CMAKE_FIND_ROOT_PATH "${FUSION_ROOT}")

set(DISABLE_ANDROID FALSE CACHE BOOL "Disable android-project build")

# SDL2
set(ENV{SDL2DIR} ${FUSION_ROOT})
include(get-sdl2.cmake)

# SDL2_image
include(get-sdl2-image.cmake)


# SDL2_ttf
include(get-sdl2-ttf.cmake)

# OPENAL
include(get-openal.cmake)

# GLM
include(get-glm.cmake)

# LUA
set(ENV{LUA_DIR} ${FUSION_ROOT})
include(get-lua.cmake)

# unless explicitly disabled, don't build android deps
if (NOT DISABLE_ANDROID)
    add_custom_command(OUTPUT android-deps
        COMMAND
            ${CMAKE_COMMAND}
            -DBOOTSTRAP_DIR=${CMAKE_SOURCE_DIR}
            -DFUSION_ROOT=${FUSION_ROOT}
            -P ${CMAKE_SOURCE_DIR}/cmake-scripts/get-android-deps.cmake
        DEPENDS lua glm openal sdl2 sdl2-image sdl2-ttf
        COMMAND
            ${CMAKE_COMMAND}
            -E touch android-deps
    )
else()
    add_custom_target(android-deps)
endif()

#FUSION
include(build-fusion-engine.cmake)

# Command to create a new game
add_custom_target(new-game
	COMMAND
		${CMAKE_COMMAND}
		-DSKEL_DIR=${CMAKE_SOURCE_DIR}/skel
		-DBOOTSTRAP_DIR=${CMAKE_SOURCE_DIR}
		-DFUSION_ROOT=${FUSION_ROOT}
        -DGAME_NAME="\${GAME_NAME}"
        -DENABLE_ANDROID="\${ANDROID}"
		-P ${CMAKE_SOURCE_DIR}/cmake-scripts/new-game.cmake
	DEPENDS fusion-engine android-deps
    )

message("")
message(STATUS "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --")
message("")
message("Summary:")
message(STATUS "Fusion root:                    ${FUSION_ROOT}/")
message(STATUS "Libraries install directory:    ${FUSION_ROOT}/lib/")
message(STATUS "Library header files:           ${FUSION_ROOT}/include/")
if (NOT DISABLE_ANDROID)
    message(STATUS "Android base project:           ${FUSION_ROOT}/android-project/")
endif()
message(STATUS "Fusion engine source:           ${FUSION_ROOT}/fusion-engine/")
message(STATUS "Path for new games:             ${FUSION_ROOT}/game/")

message("")
message("Build targets:")
message("    new-game: Build the target 'new-game' to create games")
if (NOT DISABLE_ANDROID)
    message("              - make new-game GAME_NAME=\"New Game\" ANDROID=YES")
else()
    message("              - make new-game GAME_NAME=\"New Game\"")
endif()
message("")
message("    all: Build the target 'all' to generate only the fusion root")
message("              - make all")
message("")
message("    You can come back later to generate a new game")
message("")
message(STATUS "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --")
message("")
